% collatzNodes.m
% 
% A script that evaluates the nodes of a graph of the convergence sequences
% for the Collatz Conjecture for a range of input values.
% 
% Written by Travis Llado, travis@travisllado.com, 2018-06-13

function collatzNodes(range)
    % If no arguments provided, then use default values
    if ~exist('range', 'var')
        range = 1:1000;
    end
    
    % Initialize program variables
    termLength = zeros(1,length(range));
    
    % For each input value ...
    bar = waitbar(0, 'Calculating ...');
    for ii = range
        % Only update progress bar every 1%
        if mod(ii,length(range)/100) == 0
            waitbar((ii-range(1))/length(range),bar);
        end
        
        thisNum = ii;
        while thisNum ~= 1
            if mod(thisNum,2) == 0
                thisNum = thisNum/2;
            else
                thisNum = thisNum*3 + 1;
            end
            
            termLength(ii) = termLength(ii) + 1;
        end
    end

    % Plot results
    plot(range, termLength, '.');
    xlabel('Input Value');
    ylabel('Termination Length');
    close(bar);
end

% End of File