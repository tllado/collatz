# collatz.py
# 
# A simple script that demonstrates the Collatz Conjecture, plotting the
# termination lengths for various ranges of numbers.
# 
# Takes input of two integers indicating the start and end of desired range of 
# input values, then prints the termination lengths for all numbers in the 
# desired range.
# 
# Written by Travis Llado, travis@travisllado.com, 2018-06-09

def collatz(firstNum = 1, lastNum = 100):
    import matplotlib.pyplot as mpl # Use MatPlotLib for graphics

    # Did we receive command line arguments?
    import sys
    try:
        firstNum = int(sys.argv[1])
        lastNum = int(sys.argv[2])
    except:
        pass

    # Initialize program variables
    inputNums = range(firstNum, lastNum+1)  # Reorganize input
    termLength = []                         # Initialize output
    
    # Calculate termination lengths for all input values
    for ii in range(0, len(inputNums)): # For each input value
        thisNum = inputNums[ii] # Placeholder for calculation
        termLength.append(0)    # New element for this number
        
        while thisNum != 1:     # Test until reach 1
            if thisNum%2 == 0:  # If even
                thisNum = thisNum/2
            else:               # Must be odd
                thisNum = thisNum*3 + 1
            
            termLength[ii] = termLength[ii] + 1

    # Display results
    print termLength

    mpl.plot(inputNums, termLength, '.')
    mpl.axis([inputNums[0]-1, inputNums[-1]+1, min(termLength)-1,              \
        max(termLength)+1])
    mpl.xlabel('Input Value');
    mpl.ylabel('Termination Length');
    mpl.show()

if __name__ == "__main__":
    collatz()
