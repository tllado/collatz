function [termSequence, termLength] = findTermSequence(inputNum)
    termSequence(1) = inputNum;
    termLength = 0;
    ii = 1;
    
    while inputNum ~= 1  % Test until reach 1
        if mod(inputNum,2) == 0  % If even
            inputNum = inputNum/2;
        else                    % If odd
            inputNum = inputNum*3 + 1;
        end

        ii = ii + 1;
        termSequence(ii) = inputNum;
        termLength = ii;
    end
end