% collatzGraph.m
% 
% This program draws a graph of the convergence paths of different numbers using% the Collatz Conjecture. It takes input of a vector of numbers and returns a
% plot.
% 
% Written by Travis Llado, travis@travisllado.com, 2018-06-09

function collatzGraph(range)
    % If no arguments provided, then use default values
    if ~exist('range', 'var')
        range = 1:100;
    end
    
    % Initialize program variables
    termSequence = zeros(length(range),1);
    termLength = zeros(length(range),1);
    
    % For each desired number ...
    for ii = range
        % Get termination sequence and sequence length
        [thisSequence, termLength(ii)] = findTermSequence(ii);
        
        % When sequence length is greater than array size, increase array size
        if length(thisSequence) > size(termSequence,2)
            tempSequence = termSequence;
            termSequence = zeros(length(range),size(tempSequence,2));
            termSequence(:,1:size(tempSequence,2)) = tempSequence;
        end
        
        termSequence(ii,1:length(thisSequence)) = thisSequence;
    end

    % Plot results
    hold on
    for ii = range
        plot(termSequence(ii,1:termLength(ii)),termLength(ii):-1:1);
    end
    grid on
    title('Collatz Convergence Graph');
    xlabel('Node Value');
    ylabel('Nodes Remaining');
    hold off
end

% End of File
