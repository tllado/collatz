% collatz.m
% 
% A simple script that demonstrates the Collatz Conjecture, plotting the
% termination lengths for various ranges of numbers. Takes input of a vector 
% containing the desired input values, and returns an array of termination 
% lengths for said input values and draws a plot of length vs input.
% 
% Written by Travis Llado, travis@travisllado.com, 2018-06-09

function termLength = collatzLengths(range)
    % If no arguments provided, then use default values
    if ~exist('range', 'var')
        range = 1:1000;
    end
    
    % Initialize program variables
    termLength = zeros(1,length(range));
    
    % For each input value ...
    bar = waitbar(0, 'Calculating ...');
    for ii = range
        % Only update progress bar every 1%
        if mod(ii,length(range)/100) == 0
            waitbar((ii-range(1))/length(range),bar);
        end
        
        % Calculate termination sequence length
        [~, termLength(ii)] = findTermSequence(ii);
    end

    % Plot results
    plot(range, termLength, '.');
    xlabel('Input Value');
    ylabel('Termination Length');
    close(bar);
end

% End of File
